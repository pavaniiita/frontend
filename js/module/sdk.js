/**
 * Created by pavan on 29/2/16.
 */

'use strict';
var BS = BS || {};


BS.APIPrefix = "http://localhost:8080/SuppService";

var that = {};
var BookSDK = angular.module('BookStoreSDK', []);

BookSDK.service('networkCalls', function($http, $window, $modal) {

    //code for show loader starts here
    that = this;
    this.loaderInstance = null;
    this.showLoader = function (size, templateUrl) {
        this.loaderInstance = $modal.open({
            templateUrl: templateUrl,
            controller: 'ModalLoadingCtrl',
            backdrop : false,
            size: size,
            resolve: {
            }
        });
    };
    this.hideLoader = function(){
        this.loaderInstance.dismiss('cancel');
        delete this.loaderInstance.opened;
    };

    this.showLoadingBar = false; // progress bar logic starts here.
    this.max = 200;

    this.randomStacked = function() {
        this.stacked = [];
        var types = ['success', 'info', 'warning', 'danger'];
    };
    this.randomStacked(); // progress bar logic starts here.


    // code for show loader ends here

    this.sendAjaxForGetWithData = function (pdata, pUrl, successCallback) {
        if(!this.loaderInstance || this.loaderInstance && !this.loaderInstance.opened)
            this.showLoader('lg', '../view/partials/modalTemplate.html');


        console.log(pUrl);
        $http({
            data: pdata, method: "GET", dataType: "json", url: pUrl
        }).success(function (data) {
            that.hideLoader();
            if (data.success) {
                successCallback(data);
            }
            else {
                console.log(data.message || "Unknown Server Error");
            }
        }).error(function(data, status, headers, config) {
            that.hideLoader();
            console.log(data.message || "Unknown Server Error");
        });
    };

    this.sendAjaxForGet = function ( pUrl, successCallback, failureCallback) {
        if(!this.loaderInstance || this.loaderInstance && !this.loaderInstance.opened)
            this.showLoader('lg', 'view/partials/modalTemplate.html');
        console.log(pUrl);
        $http({
            method: "GET", dataType: "json", url: pUrl, success : this.handleData
        }).success(function (data){
            that.hideLoader();
            //console.log(data);
            if (data.success==true) {
                //  console.log(data);
                successCallback(data);
            }
            else if(data.success==false) {
                if(failureCallback){
                    failureCallback(data);
                }else{
                    alert(data.message || "Unknown Server Error");
                }
            }else{
                var anchor = angular.element('<a/>');
                anchor.attr({
                    href: 'data:attachment/csv;charset=utf-8,' +encodeURI(data),
                    target: '_blank',
                    download: 'filename.csv'
                })[0].click();
                successCallback(data);
            }
        }).error(function(data, status, headers, config) {
            that.hideLoader();
            if(failureCallback)
                failureCallback(data);
            if(data && data.message)
                console.log(data.message || "Unknown Server Error");
        });
    };

    this.sendAjaxForPost = function ( pUrl, successCallback , failureCallback) {
        if(!this.loaderInstance || this.loaderInstance && !this.loaderInstance.opened)
            this.showLoader('lg', '/view/partials/modalTemplate.html');
        $http({
            method: "POST", dataType: "json", url: pUrl
        }).success(function (data) {
            that.hideLoader();
            if (data.success || failureCallback == undefined) {
                successCallback(data);
            }
            else{
                failureCallback(data);
            }
        }).error(function(data, status, headers, config) {
            that.hideLoader();
            failureCallback(data);
        });
    };

    this.sendAjaxForPostWithData = function (pdata, pUrl, successCallback, failureCallback) {
        if(!this.loaderInstance || this.loaderInstance && !this.loaderInstance.opened)
            this.showLoader('lg', '/views/partials/modalTemplate.html');
        $http({
            data: pdata, method: "POST", dataType: "json", url: pUrl
        }).success(function (data) {
            that.hideLoader();
            if (data.success) {
                successCallback(data);
            }
            else {
                //console.log(data);
                failureCallback(data);
            }
        }).error(function(data, status, headers, config) {
            that.hideLoader();
            console.log(data.message || "Unknown Server Error");
        });
    };

    this.getencodedURL = function(obj, url){
        var value = "";
        var furl = BS.APIPrefix + url + "?" + "auth_code=pavan";
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (typeof(obj[key]) == "object")
                    value = JSON.stringify(obj[key]);
                else
                    value = obj[key];
                furl += "&" + key + "=" + encodeURIComponent(value);
            }
        }
        return furl;
    };

    this.genericNetworkCall = function(obj, url, type, successCallback, failureCallback) {
        var furl =  this.getencodedURL(obj, url);

        if (type == "get")
            this.sendAjaxForGet(furl, successCallback, failureCallback);
        else if (type == "post")
            this.sendAjaxForPost(furl, successCallback, failureCallback);
    };

    this.genericNetworkCallWithData = function(payload, obj, url, type, successCallback, failureCallback) {
        var furl = this.getencodedURL(obj, url);
        if (type == "post")
            this.sendAjaxForPostWithData(payload, furl, successCallback, failureCallback);
        else if (type == "put")
            this.sendAjaxForPutWithData(payload, furl, successCallback, failureCallback);
    };

    this.getBooks=function(requestObj , successCallback, failureCallback){
        this.genericNetworkCall( requestObj , "/book", "get", successCallback, failureCallback);
    };
    this.getAllChapterOfBook=function(requestObj , successCallback, failureCallback){
        this.genericNetworkCall( requestObj , "/bookid","get", successCallback, failureCallback);
    };
    this.getChapterById=function(requestObj , successCallback, failureCallback){
        this.genericNetworkCall( requestObj , "/chapterid","get", successCallback, failureCallback);
    };

});


app.controller('ModalLoadingCtrl', function ($scope, $modalInstance) {

});

