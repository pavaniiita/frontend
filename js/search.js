/**
 * Created by pavan on 29/2/16.
 */

'use strict';
app.controller("searchCtrl",["$rootScope", "$scope","networkCalls", function($rootScope, $scope , networkCalls){
    //$scope.user = {email:"", password:""};
    //$scope.message="";
    //
    //$scope.callBackForSignIn=function(response){
    //    networkCalls.setLoggedIn(true);
    //    $location.path("/home");
    //    $rootScope.init();
    //};
    //$scope.failureCallBackForSignIn=function(response){
    //    $scope.message =  (response && response.message) || "Unable to reach server.";
    //    $scope.user={};
    //};
    //
    //$scope.signIn =function(user){
    //    networkCalls.signIn(user, $scope.callBackForSignIn , $scope.failureCallBackForSignIn );
    //};
    //if(networkCalls.isLoggedIn()){
    //    $location.path("/home");
    //}
    $scope.bookTypeahead=[];
    $scope.chapterArr=[];
    $scope.page={
        no:"",
        content:""
    };
    $scope.pageFlag=true;
    $scope.pageArr=[];

    //$scope.bookTypeahead.push()

    $scope.successCallBackForGetChapterByBookId=function(recievedObj){
        console.log(recievedObj);
        var tmp=recievedObj.data;
        for(var i=0;i<tmp.length;i++){
            var obj={
                id:tmp[i].chapterId,
                name:tmp[i].chapterName,
                no:tmp[i].chapterNum
            };
            $scope.chapterArr.push(obj);
        }
    };

    $scope.failureCallBackForGetChapterByBookId=function(recievedObj){
        console.log(recievedObj);
    };

    $scope.search=function(book){
        console.log(book);
        var id=-1;
        for(var i=0;i<$scope.bookTypeahead.length;i++){
            if($scope.bookTypeahead[i].name==book){
               id=$scope.bookTypeahead[i]._id;
                //console.log($scope.bookTypeahead[i].img);
                break;
            }
        }
        if(id==-1){
            alert("Book Not Found");
        }else{
            var obj={
                id:id
            };
            networkCalls.getAllChapterOfBook(obj, $scope.successCallBackForGetChapterByBookId , $scope.failureCallBackForGetChapterByBookId);
            console.log(id);
            $scope.pageFlag=true;
            $scope.chapterArr=[];
        }
    };
    $scope.next=function(page){
        console.log(page);
        var tmp=parseInt(page.no)-1;
        $scope.page=$scope.pageArr[tmp+1];
    };
    $scope.prev=function(page){
        console.log(page);
        var tmp=parseInt(page.no)-1;
        $scope.page=$scope.pageArr[tmp-1];
    };

    $scope.failureCallBackForGetChapterById=function(recievedObj){
        console.log(recievedObj);

    };

    $scope.successCallBackForGetChapterById=function(recievedObj){
        console.log(recievedObj);
        var tmp=recievedObj.data;
        for(var i=1;i<tmp.length;i++){
            for(var j=0;j<tmp.length;j++){
                if(i==tmp[j].pageNum){
                    var page={
                        "no":tmp[j].pageNum,
                        "content":tmp[j].content
                    };
                    $scope.pageArr.push(page);
                    break;
                }
            }
        }
        $scope.page=$scope.pageArr[0];
        $scope.starting=1;
        $scope.ending=$scope.pageArr.length;
    };

    $scope.starting;
    $scope.ending;
    $scope.currChapter="";

    $scope.viewChapter=function(chapter){
        console.log(chapter);
        $scope.currChapter=chapter.name;
        var obj={
            id:chapter.id
        };
        networkCalls.getChapterById(obj, $scope.successCallBackForGetChapterById , $scope.failureCallBackForGetChapterById);
        //   window.open("/#/view//"+test.id, '_blank');

        $scope.pageFlag=false;
        $scope.pageArr=[];
    }
    $scope.successCallBackForGetAllBooks=function(recievedObj){
            console.log(recievedObj);
            $scope.bookTypeahead=recievedObj.data;
    };

    $scope.failureCallBackForGetAllBooks= function(recievedObj){
        console.log(recievedObj);
    };
    $scope.init=function(){
        var obj={};
        networkCalls.getBooks(obj, $scope.successCallBackForGetAllBooks , $scope.failureCallBackForGetAllBooks);
    };
    $scope.init();

}]);